#ifndef GAZEBO_PLUGINS_RIDERS_WORLD_RESET
#define GAZEBO_PLUGINS_RIDERS_WORLD_RESET

#include "ros/ros.h"
#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

namespace gazebo
{

    class GAZEBO_VISIBLE WorldReset : public WorldPlugin
    {
    public:
        WorldReset();
        virtual ~WorldReset();
        virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);

    private:
        virtual void OnReset();

        std::unique_ptr<ros::NodeHandle> rosNode;
        physics::WorldPtr world;

        event::ConnectionPtr resetConnection;
    };
}
#endif