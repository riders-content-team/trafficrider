#ifndef GAZEBO_PLUGINS_LIGHT_PLUGIN_HH_
#define GAZEBO_PLUGINS_LIGHT_PLUGIN_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "ros/ros.h"

#include "std_msgs/Bool.h"
#include "std_msgs/Int16.h"


namespace gazebo
{
  class GAZEBO_VISIBLE LightPlugin : public WorldPlugin
  {
    // Constructor
    public: LightPlugin();
    
    // Deconstructor
    public: virtual ~LightPlugin();

    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
  
    private: void lightCallBack(const std_msgs::Int16::ConstPtr &msg);

    private: std::unique_ptr<ros::NodeHandle> rosNode;
  
    private: physics::WorldPtr world;
    private: physics::ModelPtr green_mesh_1;
    private: physics::ModelPtr green_mesh_2;
    private: physics::ModelPtr red_mesh_1;
    private: physics::ModelPtr red_mesh_2;
    private: ignition::math::Pose3d gm1_pose;
    private: ignition::math::Pose3d gm2_pose;
    private: ignition::math::Pose3d rm1_pose;
    private: ignition::math::Pose3d rm2_pose;

    private: ignition::math::Pose3d gm1_init_pose;
    private: ignition::math::Pose3d gm2_init_pose;
    private: ignition::math::Pose3d rm1_init_pose;
    private: ignition::math::Pose3d rm2_init_pose;

    private: ros::Subscriber sub;
    private: ros::Publisher pub;


  };
}
#endif
