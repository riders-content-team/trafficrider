#include "world_reset_plugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(WorldReset)

WorldReset::WorldReset() : WorldPlugin()
{
}

WorldReset::~WorldReset()
{
}

void WorldReset::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;

    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "jointrider_world_plugin_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to
    // the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    // Connect to the world reset event.
    this->resetConnection = event::Events::ConnectWorldReset(
        std::bind(&WorldReset::OnReset, this));
}

void WorldReset::OnReset()
{
    int model_number_to_remove = this->world->ModelCount() - 12;

    if (model_number_to_remove > 0)
    {
        for (int i = 0; i < model_number_to_remove; i++)
        {
            std::string model_name = "cylinder_" + std::to_string(i);
            std::cout << model_name << std::endl;
            this->world->RemoveModel(model_name);
        }
    }
}
