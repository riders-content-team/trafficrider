#include "LightPlugin.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(LightPlugin)


LightPlugin::LightPlugin() : WorldPlugin() // Constructor
{
   // add one cart for now - change this then change OnReset
}

LightPlugin::~LightPlugin() // Deconstructor
{
}

// Runs once on initialization
void LightPlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf){
    this->world = _world;
    this->green_mesh_1 = this->world->ModelByName("light_green_1");
    this->gm1_pose = this->green_mesh_1->WorldPose();
    this->gm1_init_pose = this ->gm1_pose;

    this->green_mesh_2 = this->world->ModelByName("light_green_2");
    this->gm2_pose = this->green_mesh_2->WorldPose();
    this->gm2_init_pose = this ->gm2_pose;

    this->red_mesh_1 = this->world->ModelByName("light_red_1");
    this->rm1_pose = this->red_mesh_1->WorldPose();
    this->rm1_init_pose = this ->rm1_pose;

    this->red_mesh_2 = this->world->ModelByName("light_red_2");
    this->rm2_pose = this->red_mesh_2->WorldPose();
    this->rm2_init_pose = this ->rm2_pose;


    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "spawn_model_service_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());
    this->pub = this->rosNode->advertise<std_msgs::Int16>("light_state",1000);
    ros::SubscribeOptions so =
    ros::SubscribeOptions::create<std_msgs::Int16>(
        "light_state",
        10,
        boost::bind(&LightPlugin::lightCallBack, this, _1),ros::VoidConstPtr(),NULL);

  this->sub = this->rosNode->subscribe(so);


}

/*topic message: (1st light) _ _ | _ _ (2nd light)
    00: light close
    01: light red
    10: light yellow
    11: light green 
    for example if message is 15 means both light is green
*/
                
void LightPlugin::lightCallBack(const std_msgs::Int16::ConstPtr &msg){
    //change positions for first lamb change y pose -0.05
    //change positions for second lamb change x pose -0.05
  if(msg->data == 13){// first lamb green, second red
    this->gm1_pose.Pos().Y() = this->gm1_init_pose.Pos().Y() - 0.05;
    this->rm1_pose.Pos().Y() = this->rm1_init_pose.Pos().Y();
    this->gm2_pose.Pos().X() = this->gm2_init_pose.Pos().X();
    this->rm2_pose.Pos().X() = this->rm2_init_pose.Pos().X() - 0.05;
  }
  else if(msg->data == 7){ // first red, second green
    this->gm1_pose.Pos().Y() = this->gm1_init_pose.Pos().Y();
    this->rm1_pose.Pos().Y() = this->rm1_init_pose.Pos().Y() - 0.05;
    this->gm2_pose.Pos().X() = this->gm2_init_pose.Pos().X() - 0.05;
    this->rm2_pose.Pos().X() = this->rm2_init_pose.Pos().X();
  }
  else if(msg->data == 15){ //both green
    this->gm1_pose.Pos().Y() = this->gm1_init_pose.Pos().Y() - 0.05;
    this->rm1_pose.Pos().Y() = this->rm1_init_pose.Pos().Y();
    this->gm2_pose.Pos().X() = this->gm2_init_pose.Pos().X() - 0.05;
    this->rm2_pose.Pos().X() = this->rm2_init_pose.Pos().X();
  }
  else if(msg->data == 5){ //both red
    this->gm1_pose.Pos().Y() = this->gm1_init_pose.Pos().Y();
    this->rm1_pose.Pos().Y() = this->rm1_init_pose.Pos().Y() - 0.05;
    this->gm2_pose.Pos().X() = this->gm2_init_pose.Pos().X();
    this->rm2_pose.Pos().X() = this->rm2_init_pose.Pos().X() - 0.05;
  }
  this->green_mesh_1->SetWorldPose(this->gm1_pose);
  this->green_mesh_2->SetWorldPose(this->gm2_pose);
  this->red_mesh_1->SetWorldPose(this->rm1_pose);
  this->red_mesh_2->SetWorldPose(this->rm2_pose);
  ros::spinOnce();
}



